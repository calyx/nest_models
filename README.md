# NestModels

Short description and motivation.

## Usage

How to use my plugin.

## Installation


    apt install libsodium23

Add this line to your application's Gemfile:

```ruby
gem 'nest_models'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install nest_models
```

## Integration with Rails

Add migrations to an app:

    bin/rails nest_models:install:migrations
    bin/rails db:migrate SCOPE=nest_models

Removing migrations

   bin/rails db:migrate SCOPE=nest_models VERSION=0

## Contributing
Contribution directions go here.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

require "nest_models/engine"

module NestModels
  def self.reserve_usernames?
    @@reserved_usernames_enabled ||= ActiveRecord::Base.connection.table_exists? 'reserved_usernames'
  end
end

#
# Original PHP: https://api.drupal.org/api/drupal/includes%21password.inc/7.x
# Port to Ruby: https://github.com/VDMi/devise-drupal
#
# This version is modified support digests with a "U" prefix
#
require 'digest/sha2'
require 'digest/md5'

module DrupalDigest
  DRUPAL_HASH_COUNT = 15
  DRUPAL_MIN_HASH_COUNT = 7
  DRUPAL_MAX_HASH_COUNT = 30
  DRUPAL_HASH_LENGTH = 55
  ITOA64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
  HASH = Digest::SHA2.new(512)

  def self.compare(hashed_password, password)
    return false if password.nil? or hashed_password.nil?
    if hashed_password[0] == "U"
      password = Digest::MD5.hexdigest password
      hashed_password = hashed_password[1..-1]
    end
    setting = hashed_password[0..11]
    if setting[0..2] != '$S$'
      return false # Wrong hash format
    end
    count_log2 = ITOA64.index(setting[3])
    if count_log2 < DRUPAL_MIN_HASH_COUNT or count_log2 > DRUPAL_MAX_HASH_COUNT
      return false
    end
    salt = setting[4..4+7]
    if salt.length != 8
      return false
    end
    count = 2 ** count_log2
    pass_hash = HASH.digest(salt + password)
    1.upto(count) do |i|
      pass_hash = HASH.digest(pass_hash + password)
    end
    hash_length = pass_hash.length
    output = setting + self._password_base64_encode(pass_hash, hash_length)
    if output.length != 98
      return false
    end
    return output[0..(DRUPAL_HASH_LENGTH-1)] == hashed_password
  end

  def self._password_base64_encode(to_encode, count)
    output = ''
    i = 0
    while true
      value = (to_encode[i]).ord
      i += 1
      output = output + ITOA64[value & 0x3f]
      if i < count
        value |= (to_encode[i].ord) << 8
      end
      output = output + ITOA64[(value >> 6) & 0x3f]
      if i >= count
        break
      end
      i += 1
      if i < count
        value |= (to_encode[i].ord) << 16
      end
      output = output + ITOA64[(value >> 12) & 0x3f]
      if i >= count
        break
      end
      i += 1
      output = output + ITOA64[(value >> 18) & 0x3f]
      if i >= count
        break
      end
    end
    return output
  end
end

if __FILE__ == $0
  puts "TESTING..."

  digests = {
    "password 1" => "$S$DJQOtUU8v1al9brfQYrNkjBx3fkjvNcMNEmeg6ld9AT7EB/Z2ym7",
    "password 2" => "$S$DhpFqxV5KfK430blGWmCb3e4aSIJMLgT6h0dl692Et3u4oRs5teD",
    "password 3" => "$S$DDJe7krxciA23ysawgpeaDxnhIozXrHynU/e481VCyp6jJJvt4Cn",
    "password 4" => "$S$DXhl/BYT9fRlUbQBbXn1Sj.P2OGJF5Y331dobIE.i3u0Avg3NQBu",
    "password 5" => "$S$DEQrlnuEFvoEahoi1n5RaAvHvXnU/WQ8I7IGDUpKddhURARM53cd",
    "password 6" => "$S$DWp0nbGgQW.UTHMvFxLV6sqxF8H1hQrjqSsATodQhCb75AWc5uB2",
    "password 7" => "$S$DuD6M7pKYUfMPKtqM1ogrNxfHTZvVZ7hGnbx43jqfjt5Hjbl83H5",
    "password 8" => "$S$DdtneWpozAXk5QfJL5Zj/QLcS2XqVV2WIZpxRtkYWwLlkYAXYJyS"
  }

  updated_digests = {
    "password 1" => "U$S$DHVrWpCJkicHzuOsxYhiUFt0ewQxMSSTUm1CE0K.vCMS/GNeHQgj",
    "password 2" => "U$S$DVBm93Qc94lAyvoyjCoBVfIiLNVa9ffo9G8678UeMV7yl0SIpT.V",
    "password 3" => "U$S$DMezNRGb3ojMIA3KV3SDiPHc98CI16TFn5OTM/BvG9fBqfY5i9wo",
    "password 4" => "U$S$D.1/islTE4wG7ZNRyFgc/N.DqBuEQznC1/PBBfczDNi.Kw/qqy7I",
    "password 5" => "U$S$DGmArN.Zv/4twg4mVIxF0zoM2KkiXRtztHqUk0L8hGYAqUWDWCmz",
    "password 6" => "U$S$D0fG/reptgCr/RY.LYCCw8rtg250XuAikRflFjru1.vQ8a1bACWc",
    "password 7" => "U$S$DJSD2mSrbiAtvmHHYahy8jtPVsDaM50wPk3IlFhzt4nsFGQ0g96b",
    "password 8" => "U$S$DIUp1fnaGeVQuIo7nO261PkzD.BUBzUwlWaZOJQ0fTNZ9zrxrvKl"
  }

  [digests, updated_digests].each do |list|
    list.each do |clear, digest|
      print digest
      print "  "
      if DrupalDigest::compare(digest, clear)
        puts "PASS"
      else
        puts "FAIL"
      end
    end
  end
end

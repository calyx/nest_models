class Conf
  @loaded = false

  def self.method_missing(method, *args)
    unless @loaded
      raise ArgumentError, 'Conf.load not yet called'
    end
    @data[method.to_s] || args.first
  end

  def self.load(*paths)
    current_path = nil
    @data = paths.inject({}) {|config, filepath|
      current_path = filepath
      if File.exist?(filepath) && settings = YAML.load_file(filepath)
        if settings && settings[Rails.env]
          config.merge(settings[Rails.env])
        else
          config
        end
      else
        config
      end
    }
    unless @data.any?
      raise StandardError, "ERROR Could not load any configuration in paths #{paths.join(', ')}"
    end
    @loaded = true
  rescue Exception => exc
    puts "ERROR parsing configuration file (#{current_path})."
    puts exc
    puts exc.callstack if exc.respond_to?(:callstack)
    exit 1
  end

  def self.data
    @data
  end
end

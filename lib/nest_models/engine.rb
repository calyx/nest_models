module NestModels
  class Engine < ::Rails::Engine
    #initializer "nest_models" do |app|
    #  Rails.backtrace_cleaner.remove_silencers!
    #  Rails.backtrace_cleaner.add_silencer do |line|
    #    (line !~ /nest_models/)
    #  end
    #end
    config.after_initialize do
      # I don't understand why it is not auto-loading
      # our custom paper trail model, but this forces it to:
      require 'paper_trail/version'
    end
  end
end

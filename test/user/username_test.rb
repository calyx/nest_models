require_relative "../test_helper"

class User::UsernameTest < ActiveSupport::TestCase

  def setup
  end

  def test_create_empty_username
    user = User.create!(password: 'imierei3bi3ooyeiBooj')
    assert user.valid?

    user = User.create!(email: 'joe.hill@iww.org', password: 'imierei3bi3ooyeiBooj')
    assert_equal "joe_hill", user.username

    user = User.create!(email: 'joe.hill@iww2.org', password: 'imierei3bi3ooyeiBooj')
    assert_not_equal "joe_hill", user.username

    user = User.create!(email: 'j@iww.org', password: 'imierei3bi3ooyeiBooj')
    assert user.username
  end

end

=begin
require_relative "../test_helper"

class User::SweeperTest < ActiveSupport::TestCase

  def test_sweep
    assert users(:sweep_admin).is_suspended?
    assert users(:sweep_user).is_suspended?
    assert users(:sweep_inactivity).is_suspended?
    assert users(:keep_inactivity).is_suspended?
    assert !users(:close_inactive).is_suspended?
    assert !users(:keep_inactive).is_suspended?
    assert !users(:keep_flag_set).is_suspended?

    UserSweeper.sweep

    assert_raises ActiveRecord::RecordNotFound do
      users(:sweep_admin).reload
    end
    assert_raises ActiveRecord::RecordNotFound do
      users(:sweep_user).reload
    end
    assert_raises ActiveRecord::RecordNotFound do
      users(:sweep_inactivity).reload
    end
    assert users(:keep_inactivity).reload

    users(:close_inactive).reload
    users(:keep_inactive).reload
    users(:keep_flag_set).reload
    assert_nil users(:close_inactive).closed_by
    assert !users(:close_inactive).is_active?
    assert users(:close_inactive).is_suspended?
    assert users(:keep_inactive).is_active?
    assert !users(:keep_inactive).is_suspended?
    assert users(:keep_flag_set).is_active?
    assert !users(:keep_flag_set).is_suspended?
  end

end
=end
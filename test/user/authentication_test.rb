require_relative "../test_helper"

require 'random_code'

class User::AuthenticationTest < ActiveSupport::TestCase

  def password1
    '8377g91udh'
  end

  def password2
    'hg923tyf38'
  end

  def test_should_require_password
    assert_no_difference "User.count" do
      u = create_user(:password => nil)
      assert u.errors.messages[:password]
    end
  end

  def test_should_not_accept_weak_passwords
    assert_no_difference "User.count" do
      u = create_user(:password => 'password12345')
      assert u.errors.messages[:password]
      assert_equal I18n.t(:not_strong_enough), u.errors.messages[:password].first
    end
    assert_no_difference "User.count" do
      u = create_user(:username => 'bamboo_lemur', :password => '12345 bamboo_lemur')
      assert u.errors.messages[:password]
      assert_equal I18n.t(:not_strong_enough), u.errors.messages[:password].first
    end
  end

  def test_should_not_accept_long_passwords
    pw = 'a5bcbe7e-5cbe-441e-a0b8-871804aa8f91ebebf1cb-604a-4d4e-884b-137e31ba5cb5'
    assert pw.length > User::MAX_PASSWORD_LENGTH
    assert_no_difference "User.count" do
      u = create_user(:password => pw)
      assert u.errors.messages[:password]
    end
  end

  def test_should_change_password
    password = 'mcbshe7849rhsgsa'
    u = create_user
    u.update_password!(password: password)
    assert_equal u, User.authenticate(u.username, password)
  end

  def test_should_authenticate_user
    u = create_user last_login_on: 1.year.ago
    assert_equal 1.year.ago.to_date, u.last_login_on
    assert_equal u, User.authenticate(u.username, u.password)
    assert_equal u.send(:fuzzy_date), u.reload.last_login_on
  end

  def test_should_fail_to_authenticate_user
    u = create_user
    assert_nil User.authenticate(u.username, "wrong password")
  end

  def test_should_authenticate_user_with_domain_on_login
    u = create_user
    assert_equal u, User.authenticate(u.email, u.password)
  end

  def test_authenticate_users_with_same_username_different_domain
    alice_foo, alice_bar = users(:alice_foo), users(:alice_bar)
    User.authenticate(alice_foo.email, alice_foo.password)
    User.authenticate(alice_bar.email, alice_bar.password)
  end

  def test_should_authenticate_service
    vpn_secret = '1123581321'
    u = create_user(:vpn_secret => vpn_secret)
    assert_equal(
      u,
      User.authenticate_service(u.email, vpn_secret, 'vpn'),
      'user should be able to authenticate with vpn_secret'
    )
    assert_equal(
      u,
      User.authenticate(u.email,u.password),
      'user with vpn secret should still be able to use password'
    )
    assert_equal(
      u,
      User.authenticate_service(u.email, u.password, 'vpn'),
      'user should be able to use main password for service auth'
    )
  end


  def test_change_password
    u = User.new(username: 'parrot', password: password1)
    #u.make_mailbox
    u.save!

    u.update_main_password!(password: password1, new_password: password2)
    u.reload
    #assert_equal "{ARGON2}" + u.primary_secret, u.mailbox.password
  end

  def test_hard_reset_password
    u = User.new(username: 'parrot', password: password1)
    u.save!
    original = u.primary_secret

    assert_raises ActiveRecord::RecordInvalid do
      u.hard_reset_password! new_password: ""
    end
    u.reload
    assert_equal original, u.primary_secret, "blank password should not be allowed"

    assert_raises ActiveRecord::RecordInvalid do
      u.hard_reset_password! new_password: "simple"
    end
    u.reload
    assert_equal original, u.primary_secret, "simple password should not be allowed"

    u.hard_reset_password! new_password: password2
    assert u.authenticated? password2
  end

  def test_recovery_code
    u = User.new(username: 'sloth', password: password1)
    #u.make_mailbox
    u.save!
    old_digest = u.password
    recovery_code = RandomCode::create(Conf.recovery_code_length, Conf.recovery_code_split)
    u.update_recovery!(password: password1, new_recovery: recovery_code)

    u.reset_password!(recovery_code: recovery_code, new_password: password2)
    u.reload
    #assert_equal "{ARGON2}" + u.primary_secret, u.mailbox.password
    assert_not_equal old_digest, u.primary_secret
  end

  def test_recovery_email
    u = User.new(username: 'pangolin', password: password1)
    u.save!
    u.recovery_email = 'root@riseup.net, collective@riseup.net, ignored@example.org'
    u.save!
    hashed_emails = u.recovery_email.split(/\s/)
    assert_equal 2, hashed_emails.size
    assert u.recovery_email_valid?('root@riseup.net')
    assert RbNaCl::PasswordHash.argon2_valid?('root@riseup.net', hashed_emails.first)
  end

  def test_recovery_token
    # even if the user has no password, we still want to be able to set a recovery token
    user = User.create(username: 'frogmouth', recovery_email: 'frogmouth@example.org', password: nil, skip_password_validation: true)
    assert user.create_recovery_token('frogmouth@example.org')
    assert user.valid?
    assert user.reload.recovery_token
  end

  def test_should_upgrade_bad_digest
    u = User.create(
      username: 'mr_bad_digest',
      email: 'mr_bad_digest@' + Conf.domain,
      primary_secret: UnixCrypt::MD5.build('password')
    )
    assert_match(/^\$1\$/, u.primary_secret)
    assert User.authenticate(u.username, 'password')
    assert_match(/^\$argon2i?d?\$/, u.reload.primary_secret)
    assert User.authenticate(u.username, 'password')
  end

  def test_should_reset_storage_key_on_hard_reset
    recovery_code = 'skgy-ad92-ay2n'
    user = create_user
    StorageKey.stub_const(:DEFAULT_MEMLIMIT, 2**13) do
      StorageKey.stub_const(:DEFAULT_OPSLIMIT, 3) do
        user.create_storage_key!(password: password1, recovery_code: recovery_code)
      end
    end
    old_public_key = user.storage_key.public_key
    user.hard_reset_password! new_password: password2, new_recovery_code: 'aaaa-aaaa-aaaa'
    assert_not_equal old_public_key, user.storage_key.public_key
  end

  def test_should_work_with_drupal_digests
    u = User.create(username: 'pumpkin', email: 'pumpkin@' + Conf.domain, primary_secret: "$S$DJQOtUU8v1al9brfQYrNkjBx3fkjvNcMNEmeg6ld9AT7EB/Z2ym7")
    assert User.authenticate(u.username, 'password 1')
  end

  protected

  def create_user(options = {})
    name = 'user_' + rand(10000000).to_s
    password = options[:passsword] || name.reverse
    User.create({
      :username => name,
      :email => [name, Conf.domain].join('@'),
      :password => password
    }.merge(options))
  end

end

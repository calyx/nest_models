=begin
require_relative "../test_helper"

class User::InviteTest < ActiveSupport::TestCase

  def password1
    '8377g91udh'
  end

  def password2
    'hg923tyf38'
  end

  def test_create_with_invite
    u = nil
    assert_nothing_raised do
      u = User.create_from_invite!(
        username: 'toucan',
        password: password1,
        password_confirmation: password1,
        invite: Invite.create!
      )
    end
    assert u.mailbox
    assert u.storage_key
  end

  def test_purge_invites
    user = users(:red)
    assert_difference 'Invite.count' do
      user.invites.create!
    end
    user.purge
    assert_equal 0, user.invites.count
  end

  # should close unused invites when user is closed
  def test_close_unused_invites
    user = users(:red)
    user.invites.clear
    i1 = user.invites.create!
    i2 = user.invites.create!
    assert_equal 2, user.invites.available.count
    i1.consume!(users(:green))
    assert_equal 1, user.invites.available.count
    user.close(nil, '')
    assert_equal 0, user.invites.available.count
    assert_equal 1, user.invites.count
    assert_equal i1, user.reload.invites.last
  end

end
=end
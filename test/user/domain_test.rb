require_relative "../test_helper"

class User::DomainTest < ActiveSupport::TestCase

  def setup
    @public = domains(:default)
    assert @public.is_public?
    @private = domains(:foo)
    assert !@private.is_public?
  end

  def test_create_private
    u = User.new(username: 'macaw', domain: @private.domain, password: "veery very seckreett")
    #u.make_mailbox
    assert_nothing_raised do
      u.save!
    end
    u.reload
    assert_equal @private.domain, u.domain
    assert u.keep_me?, "New users on a private domain should default to keep"
    #assert_equal @private.default_quota_bytes, u.mailbox.quota_bytes, "New users on private domain should use custom quota"
  end

  def test_create_public
    u = User.new(username: 'lemur', domain: @public.domain, password: "veery very seckreett")
    #u.make_mailbox
    assert_nothing_raised do
      u.save!
    end
    u.reload
    assert_equal @public.domain, u.domain
    assert !u.keep_me?, "New users on a public domain should default to not keep"
    #assert_equal Conf.default_quota.megabytes, u.mailbox.quota_bytes, "New users on public domain should use global quota."
  end

end

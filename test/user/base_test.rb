require_relative "../test_helper"

require 'random_code'

class User::BaseTest < ActiveSupport::TestCase

  def password1
    '8377g91udh'
  end

  def test_create
    u = User.new(username: 'parrot', password: password1)
    assert_nothing_raised do
      u.save!
    end
    u.reload
    assert_equal 'parrot', u.username
  end

  def test_should_create_user
    assert_difference "User.count" do
      assert create_user.valid?
    end
  end

  def test_should_require_username
    assert_no_difference "User.count" do
      u = User.create(username: "", password: password1)
      assert u.errors.messages[:username]
    end
  end

  def test_should_allow_periods_on_username
    assert_difference "User.count" do
      u = create_user(:username => 'foo.bar')
      assert u.valid?
    end
  end

  def test_should_not_allow_plus_sign_on_username
    assert_no_difference "User.count" do
      u = create_user(:username => 'foo+bar')
      assert u.errors.messages[:username]
    end
  end

  def test_should_not_allow_duplicate_usernames
    #assert_raises ActiveRecord::RecordNotUnique do
      user = create_user(username: 'red')
    #end
    assert !user.valid?
    assert_equal "has already been taken", user.errors[:username].first
  end

  def test_should_not_allow_forbidden_usernames
    user = create_user(username: 'root')
    assert !user.valid?
    assert_equal "has already been taken", user.errors[:username].first
  end

  def test_change_username
    u = User.new(username: 'auk', password: password1)
    #u.make_mailbox
    u.save!

    u = User.find_by(username: 'auk')
    u.update_username! 'auklet'
    u.reload

    assert_equal 'auklet', u.username
    #assert_equal "auklet@#{u.domain}", u.email

    #assert_equal u.username, u.mailbox.username
    #assert_equal u.email, u.mailbox.address
  end

  def test_username_conflict
    foo, bar = domains(:foo), domains(:bar)

    u1 = User.create(username: 'seagull',
                     password: password1,
                     domain:   foo.domain)
    #u1.make_mailbox
    u1.save!

    u2 = User.create(username: 'skua',
                     password: password1,
                     domain:   foo.domain)
    #u2.make_mailbox
    u2.save
    assert u2.is_active?
    #assert u2.mailbox.is_active?

    # same username different domain permitted
    u3 = User.create(username: 'seagull',
                     password: password1,
                     domain:   bar.domain)
    #u3.make_mailbox
    u3.save
    assert u2.valid?
    assert u2.is_active?
    #assert u2.mailbox.is_active?

    # same username same domain forbidden
    assert_raises StandardError do
      u2.update_username! 'seagull'
    end
    u2.reload
    assert u2.is_active?
    #assert u2.mailbox.is_active?

    #u1.mailbox.aliases.create_many ["gull@#{foo.domain}"]
    #assert_raises ActiveRecord::RecordInvalid do
    #  u2.update_username! 'gull'
    #end
    #u2.reload
    #assert u2.is_active?
    #assert u2.mailbox.is_active?

    ReservedUsername.create! username: 'pacific-gull', domain: u2.domain
    assert_raises ActiveRecord::RecordInvalid do
      u2.update_username! 'pacific-gull'
    end
    u2.reload
    assert u2.is_active?
    #assert u2.mailbox.is_active?
  end

  def test_transaction_fail
    u = User.new(username: 'built_to_fail', password: password1)
    #u.make_mailbox
    u.save!
    u.instance_eval {
      def save!
        throw :error
      end
    }
    assert_raises ArgumentError do
      u.update_username! 'nevermore'
    end
    #assert_equal 'built_to_fail', u.reload.mailbox.username
  end

  def test_forbidden_usernames
    ['root', 'r00t', 'roots', 'root0', 'administrator'].each do |username|
      assert !User.username_allowed?(username)
    end
    ['aroot', 'rcct', 'rooti'].each do |username|
      assert User.username_allowed?(username)
    end
  end

  # sometimes we add to the forbidden username list.
  # when this happens, we want to ensure that users who now have
  # forbidden usernames can still authenticate and rename their account.
  def test_rename_forbidden_username
    assert !User.username_allowed?('root')
    regexps = User.instance_variable_get('@forbidden_regexps')
    User.instance_variable_set('@forbidden_regexps', /xxxxxxxxx/)

    assert User.username_allowed?('root')
    user = User.new(username: 'root', password: password1)
    #user.make_mailbox
    user.save!

    User.instance_variable_set('@forbidden_regexps', regexps)
    assert !User.username_allowed?('root')

    user.update_username! 'tuber'
    user.reload

    assert_equal 'tuber', user.username
    #assert_equal user.username, user.mailbox.username
  end

  protected

  def create_user(options = {})
    options[:username] ||= name = 'user_' + rand(10000000).to_s
    options[:password] ||= "mdhfg763ifgnhw7"
    options[:email] ||= [options[:username], Conf.domain].join('@')
    User.create(options)
  end

end

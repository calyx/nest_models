require_relative "test_helper"

class DomainTest < ActiveSupport::TestCase

  def setup
    @foo = domains(:foo)
  end

  def test_users
    assert_equal @foo.users.sort,
                 [users(:alice_foo), users(:bob_foo), users(:alicia_foo)].sort
  end

  def test_admins
    assert_equal @foo.admins.to_a, [users(:alice_foo), users(:monkey)]
  end

  def test_domains_must_be_unique
    Domain.create!(domain: "gulls.net", max_users: 0)
    g2 = Domain.new(domain: 'gulls.net', max_users: 0)
    refute g2.valid?
  end

  def test_admins_of_groups_i_admin
    admins = [users(:alice_foo), users(:monkey)].map(&:email).sort
    assert_equal admins, users(:alice_foo).domains.admins.to_a.map(&:email).sort
    assert_equal admins, User.admins_of(users(:alice_foo).domains).to_a.map(&:email).sort
  end

end

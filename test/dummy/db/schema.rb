# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_14_155505) do

  create_table "auth_devices", force: :cascade do |t|
    t.integer "user_id"
    t.date "created_at"
    t.string "label"
    t.string "key_handle"
    t.string "public_key"
    t.text "certificate"
    t.integer "counter", default: 0
    t.index ["user_id"], name: "index_auth_devices_on_user_id"
  end

  create_table "changes", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.integer "whodunnit"
    t.text "object_changes", limit: 65535
    t.datetime "created_at"
    t.integer "domain_id"
    t.integer "user_id"
    t.string "change_name"
    t.string "memo"
    t.index ["item_type", "item_id"], name: "index_changes_on_item_type_and_item_id"
  end

  create_table "domains", force: :cascade do |t|
    t.string "domain"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "is_public"
    t.boolean "is_active"
    t.integer "max_users"
    t.integer "default_quota", limit: 8
    t.integer "storage_policy", limit: 1, default: 2
    t.integer "mfa_policy", limit: 1, default: 0
    t.boolean "allow_forward", default: true
    t.boolean "track_changes", default: false
    t.boolean "may_users_invite", default: true
    t.index ["domain"], name: "index_domains_on_domain"
  end

  create_table "ownerships", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "created_by_id"
    t.integer "domain_id"
    t.integer "admin_id"
    t.index ["domain_id", "admin_id"], name: "index_ownerships_on_domain_id_and_admin_id"
  end

  create_table "reserved_usernames", force: :cascade do |t|
    t.string "username"
    t.string "domain"
    t.boolean "api", default: false
    t.string "reserved_by"
    t.boolean "confirmed", default: false
    t.boolean "deleted", default: false
    t.date "created_on"
    t.index ["username", "domain"], name: "index_reserved_usernames_on_username_and_domain"
  end

  create_table "sessions", force: :cascade do |t|
    t.string "session_id"
    t.text "data"
    t.datetime "updated_at"
    t.datetime "created_at"
    t.integer "user_id", limit: 4
    t.integer "domain_id", limit: 4
    t.index ["session_id"], name: "index_sessions_on_session_id"
  end

  create_table "storage_keys", force: :cascade do |t|
    t.integer "enabled", limit: 1, default: 1
    t.text "public_key"
    t.integer "pwhash_algo"
    t.integer "pwhash_opslimit"
    t.integer "pwhash_memlimit"
    t.string "pwhash_salt"
    t.string "sk_nonce"
    t.string "recovery_nonce"
    t.text "locked_secretbox"
    t.text "locked_recovery_secretbox"
    t.integer "user_id"
    t.index ["user_id"], name: "index_storage_keys_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "domain"
    t.string "email", limit: 191
    t.string "display_name"
    t.date "created_on"
    t.date "updated_on"
    t.date "last_login_on"
    t.boolean "is_active", default: true
    t.boolean "is_suspended", default: false
    t.boolean "is_pending", default: false
    t.boolean "is_confirmed", default: false
    t.boolean "keep_me", default: false
    t.integer "closed_by_id"
    t.integer "closed_reason"
    t.string "primary_secret"
    t.string "vpn_secret"
    t.string "chat_secret"
    t.string "mail_secret"
    t.string "storage_secret"
    t.string "recovery_secret"
    t.string "recovery_email"
    t.string "recovery_token"
    t.datetime "recovery_token_expires_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["username", "domain"], name: "index_users_on_username_and_domain", unique: true
  end

end

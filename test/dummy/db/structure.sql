CREATE TABLE IF NOT EXISTS "users" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL,
    "username" varchar(191),
    "domain" varchar(191),
    "email" varchar(191),
    "display_name" varchar,
    "created_on" datetime,
    "updated_on" datetime,
    "last_login_on" date,
    "is_active" boolean DEFAULT 1,
    "is_suspended" boolean DEFAULT 0,
    "is_pending" boolean DEFAULT 0,
    "is_confirmed" boolean DEFAULT 0,
    "keep_me" boolean DEFAULT 0,
    "closed_by_id" integer,
    "closed_reason" integer,
    "password" varchar,
    "recovery_code" varchar,
    "recovery_email" varchar,
    "vpn_secret" varchar,
    "chat_secret" varchar,
    "mail_secret" varchar,
    "storage_secret" varchar);

CREATE UNIQUE INDEX "index_users_on_email" ON "users" ("email");

CREATE UNIQUE INDEX "index_users_on_username_and_domain" ON "users" ("username","domain");

CREATE TABLE IF NOT EXISTS "storage_keys" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL,
    "enabled" integer(1) DEFAULT 1,
    "public_key" text,
    "pwhash_algo" integer,
    "pwhash_opslimit" integer,
    "pwhash_memlimit" integer,
    "pwhash_salt" varchar,
    "sk_nonce" varchar,
    "recovery_nonce" varchar,
    "locked_secretbox" text,
    "locked_recovery_secretbox" text,
    "user_id" integer);

CREATE INDEX "index_storage_keys_on_user_id" ON "storage_keys" ("user_id");

CREATE TABLE IF NOT EXISTS "domains" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL,
    "domain" varchar,
    "created_at" datetime,
    "updated_at" datetime,
    "is_public" boolean,
    "is_active" boolean,
    "max_users" integer,
    "default_quota" integer(8),
    "storage_policy" integer(1) DEFAULT 2,
    "mfa_policy" integer(1) DEFAULT 0,
    "allow_forward" boolean DEFAULT 1,
    "track_changes" boolean DEFAULT 0,
    "may_users_invite" boolean DEFAULT 1);

CREATE INDEX "index_domains_on_domain" ON "domains" ("domain");

CREATE TABLE IF NOT EXISTS "ownerships" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL,
    "created_at" datetime,
    "updated_at" datetime,
    "created_by_id" integer,
    "domain_id" integer,
    "admin_id" integer);

CREATE INDEX "index_ownerships_on_domain_id_and_admin_id" ON "ownerships" ("domain_id","admin_id");

CREATE TABLE IF NOT EXISTS "auth_devices" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL,
    "user_id" integer,
    "created_at" date,
    "label" varchar,
    "key_handle" varchar,
    "public_key" varchar,
    "certificate" text,
    "counter" integer DEFAULT 0);

CREATE INDEX "index_auth_devices_on_user_id" ON "auth_devices" ("user_id");

CREATE TABLE IF NOT EXISTS "sessions" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL,
    "session_id" varchar,
    "data" text,
    "updated_at" datetime,
    "created_at" datetime,
    "user_id" integer(4),
    "domain_id" integer(4));

CREATE INDEX "index_sessions_on_session_id" ON "sessions" ("session_id");

CREATE TABLE IF NOT EXISTS "reserved_usernames" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL,
    "username" varchar,
    "domain" varchar,
    "api" boolean DEFAULT 0,
    "reserved_by" varchar,
    "confirmed" boolean DEFAULT 0,
    "deleted" boolean DEFAULT 0,
    "created_on" date);

CREATE INDEX "index_reserved_usernames_on_username_and_domain" ON "reserved_usernames" ("username","domain");

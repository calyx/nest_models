require_relative "test_helper"

TEST_OPSLIMIT = 3
TEST_MEMLIMIT = 2**13

#
# A class for testing that allows us access to the unencrypted private keys,
# which are normally not stored anywhere.
#
class StorageKey2 < StorageKey
  attr_reader :unencrypted_private_key
  attr_reader :last_decrypted_key
  attr_reader :last_decrypted_recovery

  protected

  def new_key
    return @unencrypted_private_key = super()
  end

  def decrypt_key(*args)
    return @last_decrypted_key = super(*args)
  end

  def decrypt_recovery(*args)
    return @last_decrypted_recovery = super(*args)
  end
end

class StorageKeyTest < ActiveSupport::TestCase
  # f-you minitest:
  self.i_suck_and_my_tests_are_order_dependent!

  PASSWORD1 = 'eat the state'

  def test_01_key_creation
    sc = nil
    assert_nothing_raised do
      sc = create PASSWORD1
    end
    assert sc
    assert_equal TEST_OPSLIMIT, sc.pwhash_opslimit
    assert_equal TEST_MEMLIMIT, sc.pwhash_memlimit
    assert_equal StorageKey::SALT_BYTES, hex_to_binary(sc.pwhash_salt).length
    assert sc.unencrypted_private_key.is_a? RbNaCl::PrivateKey
    assert sc.locked_recovery_secretbox.nil?
    assert_hex sc.locked_secretbox
    assert_hex sc.sk_nonce
    assert_equal sc.unencrypted_private_key.public_key.to_bytes, hex_to_binary(sc.public_key)

    assert_nothing_raised do
      sc = create PASSWORD1, 'random recovery code'
    end
    assert_hex sc.locked_recovery_secretbox
  end

  def test_02_key_failed_creation
    assert_raises ActiveRecord::RecordInvalid do
      create nil
    end
  end

  def test_03_change_password
    sc = create PASSWORD1

    e = assert_raises(ArgumentError) do
      sc.update_password!(password:'wrong-password', new_password:'password2')
    end
    assert_equal 'wrong password', e.to_s

    sc.update_password!(password:PASSWORD1, new_password:'password2')
    assert_equal sc.unencrypted_private_key,
                 sc.last_decrypted_key,
                 "the decrypted key should match the original key after pw change"
  end

  def test_04_reset_codes
    sc = create PASSWORD1

    # set a code
    sc.update_recovery_code_using_password!(password:PASSWORD1, new_code:'recovery-code')
    assert_equal sc.unencrypted_private_key, sc.last_decrypted_key
    assert_hex sc.locked_recovery_secretbox
    assert_not_equal sc.sk_nonce, sc.recovery_nonce

    # use the code to change password
    sc.update_password_using_recovery_code!(code:'recovery-code', new_password:'password2')
    assert_equal sc.unencrypted_private_key, sc.last_decrypted_key

    # confirm that it worked
    sc.update_password!(password:'password2', new_password:'passsword3')
    assert_equal sc.unencrypted_private_key, sc.last_decrypted_key
  end

  def create(password = nil, recovery_code = nil)
    StorageKey.stub_const(:DEFAULT_MEMLIMIT, TEST_MEMLIMIT) do
      StorageKey.stub_const(:DEFAULT_OPSLIMIT, TEST_OPSLIMIT) do
        return StorageKey2.create! user_id: 1, password: password, recovery_code: recovery_code
      end
    end
  end

  def assert_hex(str)
    assert_match(/^[0-9a-f]+$/, str, 'the string should be hex')
  end

  def hex_to_binary(hex)
    [hex].pack('H*')
  end

  def binary_to_hex(binary)
    binary.unpack('H*').first
  end
end

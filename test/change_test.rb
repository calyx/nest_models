require_relative "test_helper"

=begin
class ChangeTest < ActiveSupport::TestCase
  def setup
  end

  def test_user_versions
    user = nil
    domain = domains(:foo)
    assert domain.track_changes?, "test only works with 'track changes' on for domain"
    with_versioning do
      assert_difference "Change.count" do
        user = User.create username: "bufflehead", domain: domain.domain, password: "jhfd628w2odh"
      end
      assert_equal ::Change::USER_CREATED, Change.last.change_name
      assert_difference "Change.count" do
        user.purge
      end
      assert_equal ::Change::USER_PURGED, Change.last.change_name
    end
  end

  def test_do_not_track
    domain = domains(:default)
    assert !domain.track_changes?, "test only works with changes off for domain"
    with_versioning do
      assert_no_difference "Change.count" do
        User.create username: "bufflehead", domain: domain.domain, password: "jhfd628w2odh"
      end
    end
  end

  def test_mailbox_versions
    mb_alias = nil
    user = users(:alice_foo)
    domain = user.domain_object
    assert domain.track_changes?, "test only works with 'track changes' on for domain"
    with_versioning do
      assert_difference "Change.count", 2 do
       user.make_mailbox
       user.save
      end
      last_change = user.mailbox.versions.last
      assert_equal user, last_change.user
      assert_equal domain, last_change.domain
      assert_equal ::Change::MAILBOX_CREATED, last_change.change_name

      assert_difference "Change.count" do
       user.create_storage_key!(password: "823ufh919g1")
      end
      last_change = Change.last
      assert_equal user, last_change.user
      assert_equal domain, last_change.domain
      assert_equal ::Change::MAILBOX_ENCRYPTED, last_change.change_name

      assert_difference "Change.count" do
        mb_alias = user.mailbox.aliases.create!(source: 'red-alias@'+user.domain, dest: user.address)
      end
      last_change = Change.last
      assert_equal user, last_change.user
      assert_equal domain, last_change.domain
      assert_equal ::Change::ALIAS_CREATED, last_change.change_name

      assert_difference "Change.count" do
        mb_alias.destroy
      end
      last_change = Change.last
      assert_equal user, last_change.user
      assert_equal domain, last_change.domain
      assert_equal ::Change::ALIAS_DESTROYED, last_change.change_name
    end
  end

  private

  def with_versioning
    was_enabled = PaperTrail.enabled?
    was_enabled_for_request = PaperTrail.request.enabled?
    PaperTrail.enabled = true
    PaperTrail.request.enabled = true
    begin
      yield
    ensure
      PaperTrail.enabled = was_enabled
      PaperTrail.request.enabled = was_enabled_for_request
    end
  end

end
=end
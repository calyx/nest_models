class CreateChanges < ActiveRecord::Migration[5.2]
  def change
    create_table :changes do |t|
      t.string   "item_type", null: false
      t.integer  "item_id", null: false
      t.string   "event", null: false
      t.integer  "whodunnit"
      t.text     "object_changes", limit: 65535
      t.datetime "created_at"
      t.integer  "domain_id"
      t.integer  "user_id"
      t.string   "change_name"
      t.string   "memo"
    end
    add_index "changes", ["item_type", "item_id"]
  end
end

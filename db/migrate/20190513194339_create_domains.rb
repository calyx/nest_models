class CreateDomains < ActiveRecord::Migration[5.2]
  def change
    create_table :domains do |t|
      t.string   "domain"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "is_public"
      t.boolean  "is_active"
      t.integer  "max_users"
      t.integer  "default_quota",    limit: 8
      t.integer  "storage_policy",   limit: 1,   default: 2
      t.integer  "mfa_policy",       limit: 1,   default: 0
      t.boolean  "allow_forward",                default: true
      t.boolean  "track_changes",                default: false
      t.boolean  "may_users_invite",             default: true
    end
    add_index "domains", "domain"

    create_table "ownerships" do |t|
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "created_by_id"
      t.integer  "domain_id"
      t.integer  "admin_id"
    end
    add_index "ownerships", ["domain_id", "admin_id"]
  end
end

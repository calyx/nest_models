class CreateStorageKeys < ActiveRecord::Migration[5.2]
  def change
    create_table :storage_keys do |t|
      t.integer "enabled", limit: 1, default: 1
      t.text    "public_key"
      t.integer "pwhash_algo"
      t.integer "pwhash_opslimit"
      t.integer "pwhash_memlimit"
      t.string  "pwhash_salt"
      t.string  "sk_nonce"
      t.string  "recovery_nonce"
      t.text    "locked_secretbox"
      t.text    "locked_recovery_secretbox"
      t.integer "user_id"
    end
    add_index "storage_keys", "user_id"
  end
end

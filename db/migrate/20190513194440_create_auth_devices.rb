class CreateAuthDevices < ActiveRecord::Migration[5.2]
  def change
    create_table :auth_devices do |t|
      t.integer "user_id"
      t.date    "created_at"
      t.string  "label"
      t.string  "key_handle"
      t.string  "public_key"
      t.text    "certificate"
      t.integer "counter",  default: 0
    end
    add_index "auth_devices", "user_id"
  end
end

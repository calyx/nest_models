# options: 'CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci'

class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string   :username,     charset: 'ascii'
      t.string   :domain,       charset: 'ascii'
      t.string   :email,        charset: 'utf8mb4', limit: 191
      t.string   :display_name, charset: 'utf8mb4'
      t.date     :created_on
      t.date     :updated_on
      t.date     :last_login_on
      t.boolean  :is_active,    default: true
      t.boolean  :is_suspended, default: false
      t.boolean  :is_pending,   default: false
      t.boolean  :is_confirmed, default: false
      t.boolean  :keep_me,      default: false
      t.integer  :closed_by_id
      t.integer  :closed_reason
      t.string   :primary_secret,  charset: 'ascii'
      t.string   :vpn_secret,      charset: 'ascii'
      t.string   :chat_secret,     charset: 'ascii'
      t.string   :mail_secret,     charset: 'ascii'
      t.string   :storage_secret,  charset: 'ascii'
      t.string   :recovery_secret, charset: 'ascii'
      t.string   :recovery_email,  charset: 'ascii'
      t.string   :recovery_token,  charset: 'ascii'
      t.datetime :recovery_token_expires_at
      t.index :email, unique: true
      t.index [:username, :domain], unique: true
    end
  end
end

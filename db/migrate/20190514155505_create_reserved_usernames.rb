class CreateReservedUsernames < ActiveRecord::Migration[5.2]
  def change
    create_table :reserved_usernames do |t|
      t.string  "username"
      t.string  "domain"
      t.boolean "api", default: false
      t.string  "reserved_by"
      t.boolean "confirmed", default: false
      t.boolean "deleted", default: false
      t.date    "created_on"
    end
    add_index "reserved_usernames", ["username", "domain"]
  end
end

class CreateSessions < ActiveRecord::Migration[5.2]
  def change
    create_table :sessions do |t|
      t.string   "session_id"
      t.text     "data"
      t.datetime "updated_at"
      t.datetime "created_at"
      t.integer  "user_id",    limit: 4
      t.integer  "domain_id",  limit: 4
    end
    add_index "sessions", "session_id"
  end
end

class Change < ActiveRecord::Base
  #include ::PaperTrail::VersionConcern

  serialize :memo, Hash

  belongs_to :item, polymorphic: true
  belongs_to :user, optional: true
  belongs_to :domain, optional: true
  belongs_to :admin, optional: true, :class_name => 'User', :foreign_key => :whodunnit

  before_create :set_change_name
  alias_attribute :message, :event

  def username
    user&.address || memo["username"] || domain&.domain
  end

  USER_CREATED = "user_created"
  USER_MODIFIED = "user_modified"
  USER_DESTROYED = "user_destroyed"
  USER_SUSPENDED = "user_suspended"
  USER_PURGED = "user_purged"

  MAILBOX_CREATED = "mailbox_created"
  MAILBOX_MODIFIED = "mailbox_modified"
  MAILBOX_DESTROYED = "mailbox_destroyed"
  MAILBOX_ENCRYPTED = "mailbox_encrypted"

  ALIAS_CREATED = "alias_created"
  ALIAS_MODIFIED = "alias_modified"
  ALIAS_DESTROYED = "alias_destroyed"

  DOMAIN_CREATED = "domain_created"
  DOMAIN_MODIFIED = "domain_modified"
  DOMAIN_DESTROYED = "domain_destroyed"

  INVITE_CREATED = "invite_created"
  INVITE_MODIFIED = "invite_modified"
  INVITE_DESTROYED = "invite_destroyed"

  AUTH_FAIL = "auth_fail"
  AUTH_OK = "auth_ok"

  PASS_CHANGED = "pass_changed"
  PASS_RESET_REQUEST = "pass_reset_request"
  PASS_RESET = "pass_reset"
  PASS_2F_ENABLED = "pass_2f_enabled"
  PASS_2F_DISABLED = "pass_2f_disabled"
  PASS_2F_MODIFIED = "pass_2f_modified"

  def self.localize(change_name)
    I18n.t("change_" + (change_name || "unknown"))
  end

  def message=(value)
    self.event = value
    self.change_name = "message"
  end

  private

  #
  # We have a custom column "change_name" that is used to
  # provide the user with a more descriptive display of what type of
  # change happened.
  #
  def set_change_name
=begin
    self.change_name ||= case self.item_type
      when "User" then
        case self.event
          when "create" then Change::USER_CREATED
          when "destroy" then Change::USER_DESTROYED
          when "authfail" then Change::AUTH_FAIL
          when "authok" then Change::AUTH_OK
          else
            if self.changeset["is_suspended"]&.last == true
              Change::USER_SUSPENDED
            else
              Change::USER_MODIFIED
            end
        end
      when "StorageKey" then
        if self.event == "create"
          Change::MAILBOX_ENCRYPTED
        end
      else
        default_change_name
    end
=end
  end

  def default_change_name
    type_name = self.item_type.upcase.split("::").last
    case self.event
      when "create"  then Change::const_get("%s_CREATED" % type_name)
      when "destroy" then Change::const_get("%s_DESTROYED" % type_name)
      when "update"  then Change::const_get("%s_MODIFIED" % type_name)
    end
  end

end

#
# Join table between domains and users. If an ownership record exists, the user
# is an admin of that domain.
#
class Ownership < ActiveRecord::Base
  belongs_to :domain
  belongs_to :admin, class_name: "User"
  belongs_to :created_by, class_name: "User"
end

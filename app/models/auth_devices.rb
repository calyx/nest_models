#
# This model stores U2F device registrations used for second factor
# authentication.
#
class AuthDevices < ActiveRecord::Base
  belongs_to :user
end

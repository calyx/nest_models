class Domain < ActiveRecord::Base
  has_many :ownerships, dependent: :destroy
  has_many :admins, through: :ownerships

  validates :domain, uniqueness: true, presence: true
  validates :max_users, numericality: { less_than: 2**31 }, allow_nil: true
  validates :default_quota, numericality: { less_than: 2**63 }, allow_nil: true

  before_create :apply_defaults

  #has_paper_trail(
  #  meta: {
  #    domain_id: :id
  #  },
  #  only: [
  #    :domain, :is_public, :max_users, :is_active, :default_quota,
  #    :storage_policy, :mfa_policy, :allow_forward, :track_changes,
  #    :may_users_invite
  #  ],
  #)

  # policy options
  PERMIT  = 0
  FORBID  = 1
  REQUIRE = 2

  def to_param
    domain
  end

  def users
    User.where(domain: self.domain)
  end

  # quota represents number of bytes, stored in an 8 byte field.
  def default_quota
    read_attribute('default_quota').to_i / 1.megabyte
  end
  def default_quota_bytes
    read_attribute('default_quota')
  end
  def default_quota=(val)
    write_attribute('default_quota', [val.to_i * 1.megabyte, 2**63].min )
  end
  def default_quota_bytes=(val)
    write_attribute('default_quota', [val.to_i, 2**63].min )
  end

  private

  def apply_defaults
    self.default_quota ||= Conf.private_domain_default_quota
  end
end

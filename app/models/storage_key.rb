# == Schema Information
#
# Table name: storage_keys
#
#  id                        :integer          not null, primary key
#  enabled                   :integer          default(1)
#  public_key                :text
#  pwhash_opslimit           :integer
#  pwhash_memlimit           :integer
#  pwhash_salt               :string
#  pwhash_algo...............:string
#  sk_nonce                  :string
#  recovery_nonce            :string
#  locked_secretbox          :text
#  locked_recovery_secretbox :text
#  user_id                   :integer
#

#
# ActiveRecord support for the Dovecot TREES plugin
# https://0xacab.org/riseuplabs/trees
#
# This model holds an Curve25519 keypair, with the private key encrypted using
# a symmetric key derived from an argon2 digest of the user's password.
#
# DATABASE:
#
#   user_id: integer
#     foreign key to the user
#
#   enabled: integer(1)
#     should always be 1
#
#   public_key: text (hex encoded)
#     an ed25519 public key, hex encoded.
#
#   locked_secretbox: text (hex encoded)
#     an encrypted Curve25519 private key, hex encoded.
#     encrypted using the digest of user's password.
#
#   sk_nonce: string (hex encoded)
#     a random nonce used for creating locked_secretbox
#
#   locked_recovery_secretbox: text (hex encoded)
#     an encrypted Curve25519 private key, hex encoded.
#     encrypted using the digest of user's recovery code.
#
#   recovery_nonce: string (hex encoded)
#     a random nonce used for creating locked_recovery_secretbox
#
#   pwhash_opslimit: int
#   pwhash_memlimit: int
#   pwhash_salt: string (hex encoded)
#   pwhash_algo: 0=argon2i, 1=argon2id
#     Various required arguments for argon2
#
# USAGE:
#
#   create a new key:
#     StorageKey.create!(user_id: 1, password: '1234')
#
#   change the password:
#     key = StorageKey.find(111)
#     key.update_password!(oldpassword, newpassword)
#
#   You should only create a new key once. Changing the key will make all the
#   user's mail unreadable.
#
#   It is advisable to call update_password! in a transaction, so that the
#   other database records for the new password are atomically updated. Very
#   bad things happen if the password used to authenticate the user get out of
#   sync with the password used to encrypt the StorageKey.
#

require 'rbnacl'

class StorageKey < ActiveRecord::Base
  belongs_to :user

  ALGO_ARGON2I  = 0
  ALGO_ARGON2ID = 1
  DEFAULT_OPSLIMIT = [4, RbNaCl::PasswordHash::Argon2.opslimit_value(:interactive)].max
  DEFAULT_MEMLIMIT = [2**25, RbNaCl::PasswordHash::Argon2.memlimit_value(:interactive)].max
  DEFAULT_ALGO     = ALGO_ARGON2I
  SALT_BYTES   = RbNaCl::PasswordHash::Argon2::SALTBYTES
  NONCE_BYTES  = RbNaCl::SecretBox::NONCEBYTES
  DIGEST_BYTES = RbNaCl::SecretBox::KEYBYTES

  attr_accessor :password      # tmp virtual accessors
  attr_accessor :recovery_code # used for create

  before_validation :generate_new_keypair, :on => :create

  validates :password, presence: true, on: :create

  validates_presence_of :user_id, :public_key,
    :pwhash_opslimit, :pwhash_memlimit, :pwhash_salt,
    :sk_nonce, :locked_secretbox

  #has_paper_trail(
  #  if: proc {|sk| sk.user.domain_object&.track_changes? },
  #  meta: {
  #    user_id: proc {|sk| sk.user_id},
  #    domain_id: proc {|sk| sk.user.domain_id}
  #  },
  #  only: [:enabled]
  #)

  def enabled?
    self.enabled == 1
  end

  #
  # decrypt the key, re-encrypt with new password
  #
  def update_password!(password:, new_password:)
    if (self.locked_secretbox)
      key = self.decrypt_key(password)
      self.encrypt_key(
        key: key,
        password: new_password
      )
    else
      self.encrypt_key(
        key: self.new_key(),
        password: new_password
      )
    end
    self.save!
  end

  #
  # set a recovery code, if you have the password
  #
  def update_recovery_code_using_password!(password:, new_code:)
    if (self.locked_secretbox)
      key = self.decrypt_key(password)
      self.encrypt_recovery(
        key: key,
        recovery_code: new_code
      )
      self.save!
    end
  end

  #
  # oops! user forgot password, let them change it with a recovery code
  #
  def update_password_using_recovery_code!(code:, new_password:)
    if (self.locked_recovery_secretbox)
      key = self.decrypt_recovery(code)
      self.encrypt_key(
        key: key,
        password: new_password
      )
      self.save!
    end
  end

  protected

  #
  # generates a new keypair on create
  #
  def generate_new_keypair
    unless self.password
      self.errors.add(:password, I18n.t('errors.messages.blank'))
      raise ActiveRecord::RecordInvalid, self
    end
    key = self.new_key()
    self.encrypt_key(
      key: key,
      password: self.password
    )
    if self.recovery_code
      self.encrypt_recovery(
        key: key,
        recovery_code: self.recovery_code
      )
    end
  end

  #
  # given a private key and a password, this will encrypt the key using
  # the password and save all the necessary values into self.
  #
  def encrypt_key(key:, password: nil)
    unless key.is_a?(RbNaCl::PrivateKey)
      raise ArgumentError, "key must be an RbNaCl::PrivateKey"
    end
    if password.nil? || password.empty?
      raise ArgumentError, "password is required to encrypt the key"
    end

    # use KDF to generate a symmetric key from password
    symmetric_key = password_kdf(password)

    # encrypt the key
    self.sk_nonce = bin2hex(RbNaCl::Random.random_bytes(NONCE_BYTES))
    secret_box    = RbNaCl::SecretBox.new(symmetric_key)
    encrypted_key = secret_box.encrypt(hex2bin(self.sk_nonce), key.to_bytes)

    # save the key
    self.public_key = bin2hex(key.public_key.to_bytes)
    self.locked_secretbox = bin2hex(encrypted_key)
  end

  #
  # same as encrypt_key, but saves key encrypted using recovery_code
  # instead of password.
  #
  def encrypt_recovery(key:, recovery_code:)
    unless key.is_a?(RbNaCl::PrivateKey)
      raise ArgumentError, "key must be an RbNaCl::PrivateKey"
    end
    if recovery_code.nil? || recovery_code.empty?
      raise ArgumentError, "recovery_code is required to encrypt the key"
    end

    # use KDF to generate a symmetric key from recovery_code
    symmetric_key = password_kdf(recovery_code)

    # encrypt the key
    self.recovery_nonce = bin2hex(RbNaCl::Random.random_bytes(NONCE_BYTES))
    secret_box    = RbNaCl::SecretBox.new(symmetric_key)
    encrypted_key = secret_box.encrypt(hex2bin(self.recovery_nonce), key.to_bytes)

    # save the key
    self.public_key = bin2hex(key.public_key.to_bytes)
    self.locked_recovery_secretbox = bin2hex(encrypted_key)
  end

  #
  # reverses encrypt_key, returning RbNaCl::PrivateKey
  #
  def decrypt_key(password)
    secret_box = RbNaCl::SecretBox.new(password_kdf(password))
    return RbNaCl::PrivateKey.new(
      secret_box.decrypt(
        hex2bin(self.sk_nonce),
        hex2bin(self.locked_secretbox)
      )
    )
  rescue RbNaCl::CryptoError
    raise ArgumentError, 'wrong password'
  end

  #
  # reverses encrypt_recovery, returning RbNaCl::PrivateKey
  #
  def decrypt_recovery(recovery_code)
    secret_box = RbNaCl::SecretBox.new(password_kdf(recovery_code))
    return RbNaCl::PrivateKey.new(
      secret_box.decrypt(
        hex2bin(self.recovery_nonce),
        hex2bin(self.locked_recovery_secretbox)
      )
    )
  rescue RbNaCl::CryptoError
    raise ArgumentError, 'wrong recovery code'
  end

  #
  # generates a new Curve25519 private key
  #
  def new_key
    return RbNaCl::PrivateKey.generate
  end

  #
  # argon2 KDF
  #
  def password_kdf(secret)
    if self.pwhash_salt.nil? || self.pwhash_salt.empty?
      self.pwhash_opslimit = DEFAULT_OPSLIMIT
      self.pwhash_memlimit = DEFAULT_MEMLIMIT
      self.pwhash_algo     = DEFAULT_ALGO
      self.pwhash_salt = bin2hex(RbNaCl::Random.random_bytes(SALT_BYTES))
    end
    if self.pwhash_algo == ALGO_ARGON2ID
      RbNaCl::PasswordHash.argon2id(
        secret,
        hex2bin(self.pwhash_salt),
        self.pwhash_opslimit,
        self.pwhash_memlimit,
        DIGEST_BYTES
      )
    else
      RbNaCl::PasswordHash.argon2i(
        secret,
        hex2bin(self.pwhash_salt),
        self.pwhash_opslimit,
        self.pwhash_memlimit,
        DIGEST_BYTES
      )
    end
  end

  def hex2bin(hex)
    [hex].pack('H*')
  end

  def bin2hex(binary)
    binary.unpack('H*').first
  end

end

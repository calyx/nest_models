
class User < ActiveRecord::Base

  include UserCore
  include UserUsername
  include UserAuthentication
  include UserRecoveryEmail
  include UserStorageKey
  include UserSearch
  #include UserChanges
  include UserDomain

end

#
# All the versioning related code for a user
# included in user.rb
#

module UserChanges
  extend ActiveSupport::Concern

  included do
    has_paper_trail(
      if: proc {|u| u.domain_object&.track_changes? },
      meta: {
        user_id: :id,
        domain_id: :domain_id
      },
      only: [
        :username, :domain, :email, :recovery_email,
        :password, :keep_me, :closed_by_id,
        :is_active, :is_suspended, :is_pending, :is_confirmed
      ]
    )
  end

  class_methods do
  end

end
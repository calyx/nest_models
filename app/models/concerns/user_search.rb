#
# Code for searching users
#
# included in user.rb
#
module UserSearch
  extend ActiveSupport::Concern

  included do
    scope :most_recent, ->(limit_count) do
      order("id DESC").limit(limit_count)
    end
  end

  class_methods do
    #
    # outgoing mail is tagged with an encrypted user id.
    # this method will fetch the user record, if any, that corresponds
    # to the encrypted id
    #
    def find_by_encrypted_id(encrypted_id)
      User.find_by(id: User.connection.select_value(
        "SELECT decrypt_user_id(%s)" % User.connection.quote(encrypted_id)
      ))
    end
  end

  #
  # The user id is the preferred identifier because it is immutable.
  # When displayed publicly, user.encrypted_id should be used.
  #
  # This encrypted_id, however, can only be decrypted for a limited time
  # before the secret rotates.
  #
  def encrypted_id
    User.connection.select_value(
      "SELECT encrypt_user_id(%s)" % User.connection.quote(self.id)
    )
  end

end

#
# A recovery email allows a user to reset their password.
#
# If encrypted storage is enabled, using this method will destroy all stored
# messages.
#
# The recovery_email field may contain 1 or 2 recovery addresses, separated
# by spaces.
#
# These addresses are stored in password digest form. For older users, their
# recovery_email might still be in cleartext, so we support that too.
#

require 'random_code'
require 'validates_email_format_of'

module UserRecoveryEmail
  extend ActiveSupport::Concern

  RESET_TOKEN_LENGTH = 12
  RESET_EXPIRATION = 3

  included do
    validate :validate_recovery_email
    before_save :hash_recovery_email
  end

  #
  # creates and saves a password reset token, if and only if
  # the email matches one of the recovery emails.
  #
  # returns true if user record saved successfully
  #
  def create_recovery_token(email)
    return false unless recovery_email_valid?(email)
    update_columns(
      recovery_token: RandomCode.create(RESET_TOKEN_LENGTH),
      recovery_token_expires_at: Time.now.utc + RESET_EXPIRATION.hours
    )
  end

  #
  # returns true if the email argument matches one of our
  # saved recovery emails
  #
  def recovery_email_valid?(email)
    email = email.strip.downcase
    if recovery_email.present?
      recovery_email_array.each do |digest|
        if is_hashed?(digest)
          return true if password_matches_digest?(email, digest)
        else
          return true if digest.strip.downcase == email
        end
      end
    end
    return false
  end

  #
  # time constant comparison, but only works with tokens of length 16
  #
  def recovery_token_valid?(token)
    if token && self.recovery_token && self.recovery_token_expires_at
      test_token   = token.strip.downcase.ljust(16, "0")
      stored_token = recovery_token.downcase.ljust(16, "0")
      return RbNaCl::Util.verify16(stored_token, test_token) && self.recovery_token_expires_at > Time.now
    else
      return false
    end
  end

  protected

  #
  # only allow well formatted email addresses
  #
  def validate_recovery_email
    if recovery_email.present? && recovery_email_changed?
      recovery_email_array.each do |email|
        if ValidatesEmailFormatOf::validate_email_format(email) != nil
          errors.add :recovery_email, I18n.t('errors.messages.invalid')
          break
        end
      end
    end
  end

  #
  # create a digest of the recovery emails, so that we don't store what
  # the actual email address is.
  #
  # because the recovery_email column is a string, we can only store at most
  # two digests in the available space.
  #
  def hash_recovery_email
    if recovery_email.present? && recovery_email_changed? && !is_hashed?(recovery_email)
      self.recovery_email = recovery_email_array.map {|e|
        User.new_digest(e.strip.downcase)
      }.join(' ')
    end
  end

  private

  # allow a max of two alternate email, separated by whitespace or commas
  def recovery_email_array
    if is_hashed?(self.recovery_email)
      self.recovery_email.split(" ")[0..1]
    else
      self.recovery_email.split(/[\s,]+/)[0..1]
    end
  end

  def is_hashed?(digest)
    digest =~ /\A\$argon2i?d?\$/
  end

end
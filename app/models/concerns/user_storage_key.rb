module UserStorageKey
  extend ActiveSupport::Concern

  included do
    attr_accessor :raw_recovery_code

    has_one :storage_key, :dependent => :destroy

    before_save :create_recovery_code_digest
  end

  def encrypted_storage?
    respond_to?(:storage_key) && storage_key && storage_key.enabled?
  end

  private

  #
  # ensure recovery_code is hashed like a password
  #
  def create_recovery_code_digest
    if self.raw_recovery_code && self.raw_recovery_code
      self.recovery_code = new_digest(self.raw_recovery_code)
    end
    true
  end


end
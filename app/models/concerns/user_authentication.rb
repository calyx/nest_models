require "unix_crypt"
require "rbnacl"
require "drupal_digest"

begin
  require "zxcvbn"
rescue LoadError
end

module UserAuthentication
  extend ActiveSupport::Concern

  DIGEST_ALGO_ARGON2I  = 0
  DIGEST_ALGO_ARGON2ID = 1
  DEFAULT_DIGEST_ALGO  = DIGEST_ALGO_ARGON2I

  ##
  ## VALIDATIONS
  ##

  # Strength of 2 is "somewhat guessable: protection from unthrottled online attacks"
  MINIMUM_PW_STRENGTH_SCORE = 2

  # This is set to 60 because the SASL auth daemon can't handle longer passwords
  MAX_PASSWORD_LENGTH = 60

  included do
    has_many :sessions, :dependent => :destroy

    attr_accessor :password_confirmation
    attr_accessor :password
    attr_accessor :clear_recovery_secret
    attr_accessor :skip_password_validation

    validates :password,
      confirmation: true,
      presence: true,
      length: {maximum: MAX_PASSWORD_LENGTH},
      if: :password_required?

    #
    # Test the strength of a new password
    #
    # NOTE: this load 20mb zxcvbn dictionary each time it is run. To cache the
    # dictionary, consider Zxcvbn::Tester instead.
    #
    # zxcvbn score:
    #
    # 0 =      too guessable: risky password.
    #                         guesses < 10^3
    #
    # 1 =     very guessable: protection from throttled online attacks.
    #                         guesses < 10^6
    #
    # 2 = somewhat guessable: protection from unthrottled online attacks.
    #                         guesses < 10^8
    #
    # 3 = safely unguessable: moderate protection from offline slow-hash scenario
    #                         guesses < 10^10
    #
    # 4 =   very unguessable: strong protection from offline slow-hash scenario
    #                         guesses >= 10^10
    #
    validate :test_password_strength, :if => :password_required?

    before_save :create_password_digest, :downcase_username
    after_initialize :set_default_domain
  end

  public

  #def password=(password)
  #  if new_record?
  #    write_attribute(:password, password)
  #  else
  #    raise ArgumentError, 'attempt to change password: use update_password!()'
  #  end
  #end

  def update_password!(password:, service:nil)
    if service.nil?
      self.password = password
      self.password_confirmation = password
      # ^ note: we do not create the digest here, so that password validations
      # can run on the cleartext 'password' attr. the digest is created in a before
      # save action.
    elsif service == :mail
      self.mail_secret = new_digest(password)
    elsif (service == :vpn)
      # Currently, openvpn expects vpn_secret to be unhashed cleartext.
      self.vpn_secret = password
    elsif service == :chat
      self.chat_secret = new_digest(password)
    elsif service == :storage
      self.storage_secret = new_digest(password)
    elsif service == :recovery
      self.recovery_secret = new_digest(password)
    else
      raise ArgumentError, 'unsupported service type'
    end
    save!
  end

  #
  # A password change will alter the following db fields:
  #
  # * users.crypted_password
  # * mailbox.password
  # * storage_keys.private_key
  #
  # They must all be changed atomically, or everything goes to shit.
  #
  def update_main_password!(password:, new_password:)
    User.transaction do
      self.update_password!(password: new_password)
      #if mailbox
      #  mailbox.update_password_digest! self.crypted_password
      #end
      if encrypted_storage?
        if password.nil?
          raise ArgumentError.new('password required for update_main_password()')
        end
        storage_key.update_password!(
          password: password,
          new_password: new_password
        )
      end
    end
  end

  #
  # resets a new password using the recovery code.
  #
  def reset_password!(recovery_code:, new_password:)
    User.transaction do
      if authenticated_service?(password: recovery_code, service: :recovery)
        self.update_password!(password: new_password)
        #if mailbox
        #  mailbox.update_password_digest! self.crypted_password
        #end
        if encrypted_storage?
          storage_key.update_password_using_recovery_code!(
            code: recovery_code, new_password: new_password
          )
        end
      else
        errors.add(:base, t(:invalidate_recovery_code))
        raise ActiveRecord::RecordInvalid, self
      end
    end
  end

  #
  # This will reset an account entirely, destroying all their data
  # and creating a new storage key.
  #
  # This should only be called when there is no known password
  # or recovery code, but the user still wants the account.
  #
  def hard_reset_password!(new_password:, new_recovery_code: nil)
    User.transaction do
      self.update_password!(password: new_password)
      #if mailbox
      #  mailbox.update_password_digest! self.crypted_password
      #end
      if encrypted_storage?
        self.storage_key.destroy
        self.create_storage_key!(password: new_password)
        self.purge
      end
      if new_recovery_code
        self.update_recovery!(password: new_password, new_recovery: new_recovery_code)
      end
    end
  end

  #
  # sets a new recovery code using the current password.
  #
  def update_recovery!(password:, new_recovery:)
    User.transaction do
      if authenticated?(password)
        self.update_password!(password: new_recovery, service: :recovery)
        if encrypted_storage?
          storage_key.update_recovery_code_using_password!(
            password: password, new_code: new_recovery
          )
        end
      else
        errors.add(:base, I18n.t(:password_is_incorrect))
        raise ActiveRecord::RecordInvalid, self
      end
    end
  end

  def create_random_recovery!(password:)
    self.clear_recovery_secret = RandomCode::create(Conf.recovery_code_length || 16, Conf.recovery_code_split || 4)
    update_recovery!(password: password, new_recovery: clear_recovery_secret)
    return self.clear_recovery_secret
  end

  ##
  ## AUTHENTICATION
  ##

  def may_auth?
    is_active? && !is_suspended? && !is_pending?
  end

  #
  # Returns true if the password is correct for the user.
  #
  def authenticated?(password)
    authenticated_service? password: password, service: nil
  end

  #
  # Authenticate with a service-specific password.
  #
  # Except for :recovery service, fallback to trying the main
  # password if there is no service specific password.
  #
  # If the password is saved using an old digest, automatically
  # upgrade it.
  #
  # *All* authentication for everything happens through this method.
  #
  def authenticated_service?(password:, service:)
    service = service.to_s

    if service == 'touch'
      touch_user(password)
      return true
    end

    field = case service
      when 'vpn'       then :vpn_secret
      when 'openvpn'   then :vpn_secret
      when 'chat'      then :chat_secret
      when 'xmpp'      then :chat_secret
      when 'recovery'  then :recovery_secret
      else                  :primary_secret
    end

    digest = read_attribute(field)
    if !digest.present? && service != :recovery
      field = :primary_secret
      digest = read_attribute(field)
    end

    valid = false
    # tmp hack for old unhashed vpn_secrets
    if digest !~ /\$/ && (service == 'vpn' || service == 'openvpn')
      valid = digest == password
    else
      valid = password_matches_digest?(password, digest)
    end

    # fall back to using normal password if the service secret failed
    if !valid && field != :primary_secret
      field = :primary_secret
      digest = read_attribute(field)
      valid = password_matches_digest?(password, digest)
    end

    if valid
      if digest_needs_upgrade?(field, digest)
        upgrade_digest(field, password)
      end
      update_last_login
      return true
    else
      return false
    end
  end

  #
  # This will update the last login timestamp, but otherwise does not attempt
  # to authenticate the user unless their digest is old and needs updating.
  #
  def touch_user(password)
    field = :primary_secret
    digest = read_attribute(field)
    if digest_needs_upgrade?(field, digest) && password_matches_digest?(password, digest)
      upgrade_digest(field, password)
    end
    update_last_login
  end

  #
  # tests password strength, adds errors to user object.
  #
  def password_strong_enough?(password)
    if defined?(Zxcvbn) && password
      if Zxcvbn.test(password, [username]).score < MINIMUM_PW_STRENGTH_SCORE
        errors.add :password, I18n.t(:not_strong_enough)
        return false
      end
    end
    return true
  end

  class_methods do
    #
    # Authenticates a user given a username/email and password.
    # Returns the user if successful, or nil otherwise.
    #
    # NOTE: there is a potential timing attack here, that allows an attacker
    # to determine if the login exists as a valid user or not. Perhaps
    # this method should attempt to authenticate a dummy password even if the
    # user cannot be found.
    #
    def authenticate(login, password)
      if (login.nil? || login.empty? || password.nil? || password.empty?)
        return nil
      end

      user = self.find_by_login(login)

      if !user
        return nil
      elsif !user.may_auth?
        return nil
      elsif user.authenticated?(password)
        return user
      else
        return nil
      end
    end

    def authenticate_service(login, password, service)
      if (login.nil? || login.empty? || password.nil? || password.empty?)
        return nil
      end

      user = self.find_by_login(login)

      if !user || user.is_suspended? || !user.is_active? || user.is_pending?
        return nil
      elsif user.authenticated_service?(password: password, service: service)
        return user
      else
        return nil
      end
    end
  end

  ##
  ## UTILITY
  ##

  #
  # returns a date that is rounded to the quarter.
  #
  def fuzzy_date(date=nil)
    date  ||= Time.now.utc
    year  = date.year
    month = {
      1  => 1,  2  => 1,  3  => 1,
      4  => 4,  5  => 4,  6  => 4,
      7  => 7,  8  => 7,  9  => 7,
      10 => 10, 11 => 10, 12 => 10
    }[date.month]
    Date.new(year, month)
  end

  ##
  ## PUBLIC CLASS METHODS
  ##

  public

  class_methods do
    def new_digest(pw)
      if pw.nil?
        nil
      else
        RbNaCl::PasswordHash.argon2_str(pw)
      end
    end

    def may_auth?(login)
      User.find_by_login(login).tap {|user|
        user && user.is_active? && !user.is_suspended? && !user.is_pending?
      }
    end

    #
    # A "login" can be any number of different values:
    #
    # (1) username@domain
    # (2) username
    # (3) email
    #
    # email is not necessarily always the same as username@domain
    #
    def find_by_login(login)
      if !login.present?
        nil
      elsif login.count('@') != 0
        userpart, domainpart = login.split('@')
        self.find_by(username: userpart, domain: domainpart) ||
          self.find_by(email: login)
      else
        self.find_by(username: login, domain: Conf.domain) ||
        self.find_by(username: login, domain: nil)
      end
    end

    #
    # this is a separate method so that we can eventually support authentication
    # using aliases as well as username
    #
    def login_exists?(login)
      !! User.find_by_login(login)
    end

    #
    # tracking / logging
    #
    def log_auth_success(user, memo)
      self.log_change(user: user, event: "authok", memo: memo)
    end
    def log_auth_failure(user, memo)
      self.log_change(user: user, event: "authfail", memo: memo)
    end

    def log_change(user:, event:, memo: {})
      username = nil
      if user.is_a? String
        username = user
        user = User.find_by_login(user)
      else
        username = user.address
      end
      domain = Domain.find_by(domain: user&.domain)
      Change.create(
        event: event, user: user, domain: domain,
        item_type: "User", item_id: (user&.id || 0),
        memo: memo.merge("username" => username)
      )
    end
  end

  protected

  def test_password_strength
    self.password_strong_enough?(password)
  end

  ##
  ## CALLBACKS
  ##

  def create_password_digest
    # only create a new password digest if the password is present and
    # has not changed since the last digest was created.
    if password && password.present? && !self.authenticated?(password)
      self.write_attribute(:primary_secret, new_digest(password))
    end
  end

  def downcase_username
    write_attribute(:username, self.username.downcase)
  end

  def set_default_domain
    if has_attribute?(:domain)
      self.domain ||= Conf.domain
    end
  end

  def new_digest(pw)
    self.class.new_digest(pw)
  end

  def password_required?
    if self.skip_password_validation
      false
    else
      self.primary_secret.blank? || !self.password.nil?
    end
  end

  class_methods do
    def username_without_domain(address)
      address.split('@').first
    end
  end

  #
  # returns true if the digest was created from the password.
  #
  # Supported password digests:
  #
  # DES: "FinEQbZtgUwjY"
  # MD5: "$1$QoW/tbQq$6JVedc4LtXBZfPjCHGCPl/"
  # SHA256: "$5$7FiVVyf8WlpE7CJb$YCgaoF8oUUxsw.cd9lmKVha8QSVPZeumbC3CUqiHG32"
  # SHA512: "$6$OUMR0gS2w9uJdnG8$yL0z8/y04f9NE7Vsi2tYbLtdhQm1.j2n7xfyHhmU4b..."
  # ARGON2: "$argon2i$v=19$m=32768,t=4,p=1$aRJ9T7ZVYTVIpkJCbCy9bw$hWiOb6QwZ591nV8tXAfqSLuPD8yqI+ZN/B7ajmU33Zc"
  # DRUPAL: "$S$DJQOtUU8v1al9brfQYrNkjBx3fkjvNcMNEmeg6ld9AT7EB/Z2ym7"
  #
  def password_matches_digest?(password, digest)
    password = (password || "").to_s
    if digest.nil?
      false
    elsif digest =~ /\A\$[156]\$/ || digest !~ /\$/
      UnixCrypt.valid?(password, digest)
    elsif digest =~ /\A\$argon2id?\$/
      RbNaCl::PasswordHash.argon2_valid?(password, digest)
    elsif digest[0..2] == "$S$"
      DrupalDigest::compare(digest, password)
    else
      false
    end
  end

  def digest_needs_upgrade?(field, digest)
    digest !~ /\A\$argon2id?\$/
  end

  #
  # This must only be called after we have validated that the
  # password is correct.
  #
  def upgrade_digest(field, password)
    self.update_column(field, new_digest(password))
  end

  # TODO
  #
  # override AuthenticatedUser upgrade_digest to also copy digest to
  # mailbox record.
  #
  #def upgrade_digest(field, password)
  #  super(field, password)
  #  if field == :crypted_password && self.mailbox
  #    self.mailbox.update_password_digest! self.crypted_password
  #  end
  #end

  #
  # update last login date after successful authentication.
  #
  def update_last_login
    new_date = fuzzy_date
    unless self.last_login_on == new_date
      if self.new_record?
        self.last_login_on = new_date
      else
        update_column(:last_login_on, new_date)
      end
    end
  end

end

#
# Domain related code for a user.
# included in user.rb
#

module UserDomain
  extend ActiveSupport::Concern

  included do
    has_many :ownerships, :foreign_key => 'admin_id', :dependent => :destroy
    has_many :domains, through: :ownerships do
      # admins for domains that this user also admins
      def admins
        User.admins_of(proxy_association.owner.domains)
      end
    end

    scope :admins_of, ->(domains) { joins(:ownerships).where('ownerships.domain_id IN (?)', domains.pluck(:id)).distinct }

    before_create :apply_domain_defaults
  end

  def is_admin?
    is_superadmin? || ownerships.any?
  end

  def admin_of?(domain)
    is_superadmin? || ownerships.any? {|o| o.domain == domain}
  end

  def admin_of_domains
    is_superadmin? ? Domain.all : self.domains
  end

  def domain_id
    domain_object&.id
  end

  def domain_object
    @domain_object ||= Domain.find_by(domain: domain)
  end

  def may_set_external_forward?
    domain_object.allow_forward?
  end

  def may_encrypt_storage?
    domain_object.storage_policy != Domain::FORBID
  end

  def must_encrypt_storage?
    domain_object.storage_policy == Domain::REQUIRE
  end

  def may_enable_mfa?
    domain_object.mfa_policy != Domain::FORBID
  end

  #
  # mfa must be enabled if it is required for the user's domain, or any
  # domain that the user is an administrator of.
  #
  def must_enable_mfa?
    domain_object.mfa_policy == Domain::REQUIRE || self.domains.exists?(mfa_policy: Domain::REQUIRE)
  end

  protected

  #
  # apply different default user values depending on
  # what options are configured in the domain.
  #
  def apply_domain_defaults
    if domain_object
      self.keep_me = !domain_object.is_public?
    end
    true
  end

end
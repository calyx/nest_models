#
# A module to add super simple callback hooks, with arguments.
#

module SimpleHooks
  extend ActiveSupport::Concern

  included do
    class_attribute :__hooks
  end

  def run_hooks(hook_name, *args)
    self.class._run_hooks(self, hook_name, *args)
  end

  class_methods do
    def add_hook(hook_name, method_name=nil, &block)
      self.__hooks[hook_name.to_sym] << begin
        if block_given?
          block
        elsif method_name
          method_name
        else
          raise ArgumentError, 'method_name or block must be given'
        end
      end
    end

    def define_hooks(*hooks)
      self.__hooks ||= {}
      hooks.each do |hook|
        self.__hooks[hook.to_sym] ||= []
      end
    end

    def run_hooks(hook_name, *args)
      self._run_hooks(self, hook_name, *args)
    end

    def _run_hooks(object, hook_name, *args)
      hooks = self.__hooks[hook_name.to_sym]
      hooks.each do |hook|
        if hook.is_a?(Proc)
          hook.call(*args)
        elsif object.respond_to?(hook, true)
          object.send(hook, *args)
        end
      end
    end

  end
end
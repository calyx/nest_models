module UserCore
  extend ActiveSupport::Concern

  ACTIVE  = 'active'
  CLOSED  = 'closed'
  PENDING = 'pending'

  included do
    include SimpleHooks
    define_hooks :build, :close, :purge

    validate :validate_username

    before_create :fuzz_creation_date
    before_save :update_email, :fuzz_updated_date
  end

  def status
    if is_pending?
      PENDING
    elsif is_suspended?
      CLOSED
    elsif is_active?
      ACTIVE
    else
      CLOSED
    end
  end

  def closed_by_inactivity?
    !is_active? && is_suspended? && closed_by_id.nil?
  end

  def closed_by_admin?
    !is_active? && is_suspended? && closed_by_id != self.id
  end

  def closed_by_self?
    !is_active? && is_suspended? && closed_by_id == self.id
  end

  def address
    [username, '@', domain].join
  end
  alias_method :display_name, :address

  def login
    domain ? address : username
  end

  def is_superadmin?
    Conf&.admins&.each do |login|
      if login =~ /@/
        return true if login == self.address
      elsif Conf.domain
        return true if login == self.username && Conf.domain == self.domain
      else
        return true if login == self.username
      end
    end
    return false
  end

  #
  # create a mailbox record
  #
  # This should be the only place that a mailbox gets built.
  #
  #def make_mailbox
  #  create_password_digest unless crypted_password
  #  build_mailbox username: username,
  #                domain:   domain,
  #                password: crypted_password
  #end

  #
  # Administratively suspend this account
  #
  def close(closed_by, closing_note=nil)
    User.transaction do
      self.closed_by = closed_by
      self.closing_note = closing_note
      self.is_active = false
      self.is_suspended = true
      #if mailbox
      #  mailbox.close
      #end
      run_hooks(:close)
      save(validate: false)
    end
  end

  #
  # Restore a suspended account. Does do anything to
  # pending accounts.
  #
  def restore
    return if self.is_pending?
    User.transaction do
      self.closed_by = nil
      self.closing_note = nil
      self.is_active = true
      self.is_suspended = false
      #if mailbox
      #  mailbox.restore
      #end
      save
    end
  end

  #
  # Temporarily make this account inactive and pending.
  #
  def pause
    User.transaction do
      write_attribute(:is_active, false)
      write_attribute(:is_pending, true)
    end
  end

  #
  # makes an account active unless it is inactive because of suspension
  #
  def activate
    unless self.is_suspended? || self.is_pending?
      update_attribute(:is_active, true)
    end
  end

  #
  # purge all user data
  #
  def purge(admin: self)
    return if self.is_pending?
    User.transaction do
      #if self.mailbox
      #  self.is_pending = true
      #  self.is_active = false
      #  self.mailbox.purge
      #end
      run_hooks(:purge)
      self.save(validate: false)
    end
    if self.domain_object&.track_changes?
      PaperTrail::Version.create(item_type: "User", item_id: self.id,
        event: "update", change_name: Change::USER_PURGED,
        admin: admin, domain_id: self.domain_id)
    end
  end

  ##
  ## STUBS: overridden by other concerns, but here in core
  ## in case those other concerns are not loaded
  ##
  def encrypted_storage?
    false
  end

  protected

  ##
  ## CALLBACKS
  ##

  def update_email
    self.email ||= [username, domain].join '@'
  end

  #
  # keep less accurate dates
  #
  def fuzz_creation_date
    self.created_on = fuzzy_date
    true
  end
  def fuzz_updated_date
    if is_suspended?
      self.updated_on = Time.now.utc.to_date
    else
      self.updated_on = fuzzy_date
    end
    true
  end

  class_methods do
  end

end

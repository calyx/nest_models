#
# Restrictions on user.username
#
# included in user.rb
#
module UserUsername
  extend ActiveSupport::Concern

  # don't change this without also modifying the users.username column width:
  MAX_USERNAME_LENGTH = 40

  # require number or letter as first digit, don't allow two symbols in a row
  USERNAME_RE = /\A[a-z0-9]+([-_\.]?[a-z0-9])+\z/

  included do
    before_validation :generate_username

    validates :username,
      presence: true,
      format: {with: USERNAME_RE},
      length: {in: 2..MAX_USERNAME_LENGTH}

    validate :validate_username
  end

  def username=(username)
    if new_record?
      write_attribute(:username, username)
    else
      raise ArgumentError, 'attempt to change username: use update_username!()'
    end
  end

  #
  # A username change will alter the following db fields:
  #
  # * users.username
  # * users.email
  # * mailboxes.username
  # * mailboxes.address
  # * aliases.source
  #
  # They must all be changed atomically, or everything goes to shit.
  #
  def update_username!(new_username)
    if self.username == new_username || !new_username.present?
      return true
    else
      User.transaction do
        #if mailbox && mailbox.username != new_username
        #  self.pause # unpaused with EnableUserTask
        #  mailbox.update_username! new_username
        #end
        write_attribute(:username, new_username)
        save!
      end
    end
  end

  class_methods do

    def username_valid?(username)
      if !username_allowed?(username)
        false
      elsif username_taken?(username)
        false
      elsif username.length > MAX_USERNAME_LENGTH
        false
      elsif username !~ USERNAME_RE
        false
      else
        true
      end
    end

    #
    # returns true if the username is not in the list of forbidden usernames, or
    # ASCII homographs of forbidden usernames.
    #
    def username_allowed?(username)
      if Conf.forbidden_usernames
        @forbidden_regexps ||= begin
          Regexp.union(
            Conf.forbidden_usernames([]).map { |uname|
              Regexp.new(
                "\\A" +
                  uname.
                  gsub(/[o0]/, '[o0]').
                  gsub(/[i1l]/, '[i1l]').
                  gsub(/[_-]/, "[\\._-]").
                  gsub(/\*/, '.*') +
                "[s0-9_-]*\\z"
              )
            }
          )
        end
        !@forbidden_regexps.match(username)
      else
        true
      end
    end

    def username_taken?(username, domain=nil)
      if NestModels.reserve_usernames?
        return true if ReservedUsername.find_by(username: username, domain: domain)
      end
      return !!User.find_by(username: username, domain: domain)
      # TODO:
      #if self.mailbox && self.mailbox.id
      #  return true if Mailbox::Alias.where(
      #                   'source = ? AND mailbox_id != ?',
      #                   self.address,
      #                   self.mailbox.id
      #                 ).any?
      #else
      #  return true if Mailbox::Alias.find_by(source: self.address)
      #end
    end

  end

  private

  RANDOM_USERNAME_CHARSET = %w[a b c d e f g h i j k m n p q r s t u v w x y z 2 3 4 5 6 7 8 9]

  def generate_username
    if self.username.nil?
      if email
        uname = email.split('@').first
        uname = I18n.transliterate(uname).downcase
        uname = uname.gsub(/[^a-z0-9_-]/, '_').gsub(/_+/, '_')
      end
      while uname.nil? || User.username_taken?(uname, self.domain) || !User.username_valid?(uname)
        uname = SecureRandom.random_bytes(6).each_byte.map {|byte|
          RANDOM_USERNAME_CHARSET[byte % RANDOM_USERNAME_CHARSET.length]
        }.join
      end
      write_attribute(:username, uname)
    end
  end

  def validate_username
    if username_changed?
      # update_email if self.respond_to?(:email) TODO: if and only if email is linked to username
      if defined?(Domain) && Domain.find_by(domain: self.domain).nil?
        errors.add :base, I18n.t('domain_invalid')
      end
      if User.username_taken?(self.username, self.domain) && errors[:username].empty?
        errors.add 'username', I18n.t('errors.messages.taken')
      end
      if !User.username_allowed?(self.username) && errors[:username].empty?
        errors.add 'username', I18n.t('errors.messages.taken')
      end
    end
  end

end
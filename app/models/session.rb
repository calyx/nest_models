#
# see:
# * https://github.com/rails/activerecord-session_store
# * config/initializers/session_store.rb
#
if defined?(ActiveRecord::SessionStore)
  class ::Session < ActiveRecord::SessionStore::Session
    belongs_to :user, optional: true
    belongs_to :domain, optional: true

    def self.sweep
      user_session_expiry        = nil
      user_session_max_lifetime  = nil
      admin_session_expiry       = nil
      admin_session_max_lifetime = nil
      if defined?(Conf)
        user_session_expiry        = words_to_time Conf.user_session_expiry
        user_session_max_lifetime  = words_to_time Conf.user_session_max_lifetime
        admin_session_expiry       = words_to_time Conf.admin_session_expiry
        admin_session_max_lifetime = words_to_time Conf.admin_session_max_lifetime
      end
      user_session_expiry        ||= words_to_time "2 hours"
      user_session_max_lifetime  ||= words_to_time "6 hours"
      admin_session_expiry       ||= words_to_time "4 hours"
      admin_session_max_lifetime ||= words_to_time "12 hours"

      Session.where(
        "is_admin = ? AND (updated_at < ? OR created_at < ?)",
        false, user_session_expiry, user_session_max_lifetime
      ).delete_all
      Session.where(
        "is_admin = ? AND (updated_at < ? OR created_at < ?)",
        true, admin_session_expiry, admin_session_max_lifetime
      ).delete_all
    end

    private

    def serialize_data!
      self.user_id ||= data["user_id"]
      if self.user_id && self.domain_id.nil?
        domain = User.find(user_id)&.domain
        if domain
          self.domain_id = Domain.find_by(domain: domain)&.id
        end
      end
      super
    end

    def self.words_to_time(str)
      Time.now.utc - str.split.inject { |count, unit| count.to_i.send(unit) }
    rescue
      nil
    end
  end
end

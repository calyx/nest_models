$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "nest_models/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "nest_models"
  spec.version     = NestModels::VERSION
  spec.authors     = ["elijah"]
  spec.email       = ["elijah@riseup.net"]
  spec.homepage    = "https://0xacab.org/calyx"
  spec.summary     = "Summary of NestModels."
  spec.description = "Description of NestModels."
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 5.2.5"
  spec.add_dependency "rbnacl", "~> 7.1.1"
  spec.add_dependency "zxcvbn-ruby", "~> 0.1"
  spec.add_dependency "bcrypt", "~> 3.1"
  spec.add_dependency "unix-crypt", "~> 1.3.0"
  spec.add_dependency "paper_trail", "~> 12.0"
  spec.add_dependency "validates_email_format_of", "~> 1.6.3"

  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency "byebug"
  spec.add_development_dependency "minitest-stub-const" # adds stub_const()
end
